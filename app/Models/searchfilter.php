<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class searchfilter extends Model
{
    //
     protected $table = 'searchfilters';
    protected $primaryKey = 'id';

    protected $fillable = ['id','professional_location','project_type','construction_type','plot_location','plot_size','covered_area','profession'];
}
