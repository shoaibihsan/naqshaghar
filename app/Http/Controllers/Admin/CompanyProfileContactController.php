<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CompanyProfileContactDetail;
use Illuminate\Support\Facades\Input;
use App\companyprofile;

class CompanyProfileContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_profile_contact=CompanyProfileContactDetail::with('company')->paginate(20);
        return view('Admin.company.company-profile.profile-contact-detail.index', compact('company_profile_contact'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companyprofile = companyprofile::all();
        return view('Admin.company.company-profile.profile-contact-detail.create',compact('companyprofile'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cpcd = new CompanyProfileContactDetail();
        $cpcd->company_profile_id = $request->company_profile_id;
        $cpcd->c_created_at = $request->c_created_at;
        $cpcd->c_fullname = $request->c_fullname;
        $cpcd->c_address = $request->c_address;
        $cpcd->c_owner_name = $request->c_owner_name;
        $cpcd->c_mobile_number = $request->c_mobile_number;
        $cpcd->c_email = $request->c_email;
        
        $cpcd->save();
        return redirect()->route('company-profile-contact.index');
        return back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companyprofile = companyprofile::all();
        $company_profile_contact = CompanyProfileContactDetail::find($id);
        return view('Admin.company.company-profile.profile-contact-detail.create',compact('company_profile_contact','companyprofile'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cpcd = CompanyProfileContactDetail::find($id);
        $cpcd->company_profile_id = $request->company_profile_id;
        $cpcd->c_created_at = $request->c_created_at;
        $cpcd->c_fullname = $request->c_fullname;
        $cpcd->c_address = $request->c_address;
        $cpcd->c_owner_name = $request->c_owner_name;
        $cpcd->c_mobile_number = $request->c_mobile_number;
        $cpcd->c_email = $request->c_email;
        
        $cpcd->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arch = CompanyProfileContactDetail::find($id)>detele();
        return back()>with('message','Record deleted successfully ');
    }
}
