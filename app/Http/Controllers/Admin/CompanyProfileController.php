<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\companyprofile;
use Illuminate\Support\Facades\Input;
use App\Models\Company;

class CompanyProfileController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data['company_profile']=companyprofile::with('company')->paginate(20);
        //dd($data['company_profile']);
        return view('Admin.company.company-profile.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Company::all();
        // dd($company);
        return view('Admin.company.company-profile.create',compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company_profile = new companyprofile();
        $company_profile->company_id = $request->company_id;

        if ($request->file('cp_slider_image')) {
            $file = $request->cp_slider_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $company_profile->cp_slider_image = $filename;
            $path = public_path('images/cp_slider_images');
            $file->move($path, $filename);
        }

        $company_profile->cp_about_us = $request->cp_about_us;
        $company_profile->save();
        return redirect()->route('company-profile.index');
        return back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::all();
        $company_profile = companyprofile::find($id);
        return view('Admin.company.company-profile.create',compact('company','company_profile'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company_profile = companyprofile::find($id);
        $company_profile->company_id = $request->company_id;

        if ($request->file('cp_slider_image')) {
            $file = $request->cp_slider_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $company_profile->cp_slider_image = $filename;
            $path = public_path('images/cp_slider_images');
            $file->move($path, $filename);
        }

        $company_profile->cp_about_us = $request->cp_about_us;
        $company_profile->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arch = companyprofile::find($id)>detele();
        return back()>with('message','Record deleted successfully ');
    }
}
