<?php

namespace App\Http\Controllers\Website;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Artitecture;
use App\Models\Construction;
use App\Models\Intereior;
use App\Models\Landscape;
use App\Models\TownPlanning;
use App\companyprofile;
use App\Models\CompanyProfileContactDetail;
use App\Models\CompanyProfileGallery;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function index(){
      $data['ar'] = Artitecture::all()->count();
      $data['con'] = Construction::all()->count();
      $data['in'] = Intereior::all()->count();
      $data['land'] = Landscape::all()->count();
    	return view('website.index',compact('data'));
    }
    public function about(){
    	return view('website.about-us');
    }
    public function term_condition(){
    	return view('website.term-condition');
    }
    public function faq(){
    	return view('website.faq');
    }
    public function blogs(){
    	return view('website.blogs');
    }
   public function naqsha_search()
   {
    echo "string";
   }
    public function architecture(Request $request){
         $cc = Artitecture::all();

         $data = array();
         if(isset($cc) && count($cc) > 0){
          foreach ($cc as $key => $value) {
            array_push($data,[
                'pro_location' => $value->professional_location,
                'plot_type'=>$value->plot_type,
                'no_floor'=>$value->no_floor,
                'plot_location' => $value->plot_location,
                'plot_size' => $value->plot_size
              ]);
          }
         }

        $datas = Artitecture::with('company');
        if(isset($request->professional_location) && !is_null($request->professional_location) && $request->professional_location != ''){
          $datas = $datas->where('professional_location',$request->professional_location);
        }
        if(isset($request->plot_type) && !is_null($request->plot_type) && $request->plot_type != ''){
          $datas = $datas->where('plot_type',$request->plot_type);
        }
        if(isset($request->no_floor) && !is_null($request->no_floor) && $request->no_floor != ''){
          $datas = $datas->where('no_floor',$request->no_floor);
        }
        if(isset($request->plot_location) && !is_null($request->plot_location) && $request->plot_location != ''){
          $datas = $datas->where('plot_location',$request->plot_location);
        }
        if(isset($request->plot_size) && !is_null($request->plot_size) && $request->plot_size != ''){
          $datas = $datas->where('plot_size',$request->plot_size);
        }
        $cc = $datas->paginate(5);
      

      return view('website.architecture', compact('cc','data'));
    }
    public function construction(Request $request){
        $cc=Construction::all();

        $data = array();
         if(isset($cc) && count($cc) > 0){
          foreach ($cc as $key => $value) {
            array_push($data,[
                'pro_location' => $value->professional_location,
                'plot_location' => $value->plot_location,
                'plot_size' => $value->plot_size,
                'covered_area' => $value->covered_area,
                'package' => $value->package,
                'plot_type'=>$value->type,
                'no_floor'=>$value->no_floor,

              ]);
          }
         }

         $datas = Construction::with('company');
        if(isset($request->professional_location) && !is_null($request->professional_location) && $request->professional_location != ''){
          $datas = $datas->where('professional_location',$request->professional_location);
        }
        if(isset($request->plot_type) && !is_null($request->plot_type) && $request->plot_type != ''){
          $datas = $datas->where('type',$request->plot_type);
        }
        if(isset($request->no_floor) && !is_null($request->no_floor) && $request->no_floor != ''){
          $datas = $datas->where('no_floor',$request->no_floor);
        }
        if(isset($request->plot_location) && !is_null($request->plot_location) && $request->plot_location != ''){
          $datas = $datas->where('plot_location',$request->plot_location);
        }
        if(isset($request->plot_size) && !is_null($request->plot_size) && $request->plot_size != ''){
          $datas = $datas->where('plot_size',$request->plot_size);
        }

        if(isset($request->covered_area) && !is_null($request->covered_area) && $request->covered_area != ''){
          $datas = $datas->where('covered_area',$request->covered_area);
        }
        if(isset($request->plot_type) && !is_null($request->plot_type) && $request->plot_type != ''){
          $datas = $datas->where('plot_type',$request->plot_type);
        }
        $cc = $datas->get();
        
        // dd($cc,$request->all());


        return view('website.construction',compact('cc','data'));
    }
    public function interior(Request $request){
        $cc=Intereior::all();

        $data = array();
         if(isset($cc) && count($cc) > 0){
          foreach ($cc as $key => $value) {
            array_push($data,[
                'pro_location' => $value->professional_location,
                'plot_location' => $value->plot_location,
                'plot_type'=>$value->plot_type,
                'covered_area' => $value->covered_area,
              ]);
          }
         }
         
        $datas = Intereior::with('company');
        if(isset($request->professional_location) && !is_null($request->professional_location) && $request->professional_location != ''){
          $datas = $datas->where('professional_location',$request->professional_location);
        }
        if(isset($request->plot_type) && !is_null($request->plot_type) && $request->plot_type != ''){
          $datas = $datas->where('type',$request->plot_type);
        }
       
        if(isset($request->plot_location) && !is_null($request->plot_location) && $request->plot_location != ''){
          $datas = $datas->where('plot_location',$request->plot_location);
        }
      

        if(isset($request->covered_area) && !is_null($request->covered_area) && $request->covered_area != ''){
          $datas = $datas->where('covered_area',$request->covered_area);
        }
        if(isset($request->plot_type) && !is_null($request->plot_type) && $request->plot_type != ''){
          $datas = $datas->where('plot_type',$request->plot_type);
        }
        $cc = $datas->get();

        return view('website.interior',compact('cc','data'));
    }
    public function landscape(Request $request){
        $cc=Landscape::all();

        $data = array();
         if(isset($cc) && count($cc) > 0){
          foreach ($cc as $key => $value) {
            array_push($data,[
                'pro_location' => $value->professional_location,
                'plot_type'=>$value->plot_type,
                'plot_location' => $value->plot_location,
                'covered_area' => $value->covered_area,
              ]);
          }
         }
         
           $datas = Landscape::with('company');
        if(isset($request->professional_location) && !is_null($request->professional_location) && $request->professional_location != ''){
          $datas = $datas->where('professional_location',$request->professional_location);
        }
        if(isset($request->plot_type) && !is_null($request->plot_type) && $request->plot_type != ''){
          $datas = $datas->where('type',$request->plot_type);
        }
       
        if(isset($request->plot_location) && !is_null($request->plot_location) && $request->plot_location != ''){
          $datas = $datas->where('plot_location',$request->plot_location);
        }
      

        if(isset($request->covered_area) && !is_null($request->covered_area) && $request->covered_area != ''){
          $datas = $datas->where('covered_area',$request->covered_area);
        }
        if(isset($request->plot_type) && !is_null($request->plot_type) && $request->plot_type != ''){
          $datas = $datas->where('plot_type',$request->plot_type);
        }
        
        $cc = $datas->get();

        return view('website.landscape',compact('cc','data'));
    }
    public function town_planner(Request $request){
        $cc=TownPlanning::all();

        $data = array();
         if(isset($cc) && count($cc) > 0){
          foreach ($cc as $key => $value) {
            array_push($data,[
                'pro_location' => $value->professional_location,
                'plot_location' => $value->town_location,
                'covered_area' => $value->covered_area,
              ]);
          }
         }

        return view('website.town-planner',compact('cc','data'));
    }
    public function engineers(){
        return view('website.engineers');
    }

   public function gallery_type(){
    return view('website.gallery.gallery-type');
   }
   public function kanal_2_residential(){
    return view('website.gallery.kanal_2_residential');
   }
   public function d3_evlevation(){
    return view('website.gallery.d3-evlevation');
   }
   public function one_kanal_house(){
    return view('website.gallery.one-kanal-house');
   }

   public function interior_gallery_type(){
    return view('website.gallery.interior-gallery-type');
   }
   public function commercial_interior(){
    return view('website.gallery.commercial-interior');
   }
   public function residential_interior(){
    return view('website.gallery.residential-interior');
   }
   public function landscap_gallery(){
    return view('website.gallery.landscap-gallery');
   }
   public function landscapes(){
    return view('website.gallery.landscape');
   }
   // public function kanal_2_residential(){
   //  return view('website.gallery.kanal_2_residential');
   // }


   public function company_detail($id){
      $data = [];
      $cp = companyprofile::where('company_id',$id)->first();

      if(!is_null($cp) && $cp->id > 0){
        $data['cpcd'] = CompanyProfileContactDetail::where('company_profile_id',$cp->id)->first();
        $data['cpg'] = CompanyProfileGallery::where('company_profile_id',$cp->id)->get();
      }

      return view('website.company-detail',compact('cp','data'));
   }

   public function sendEmail(Request $request)
  {
      $data = [
            'no-reply' => 'contact-from-web@nomail.com',
            'admin'    => 'mohamedsasa201042@yahoo.com',
            'full_name'    => $request->get('full_name'),
            'email'    => $request->get('email'),
            'Email'    => 'mohamedsasa201042@yahoo.com',
            'mobile'    => $request->get('mobile'),
            'company_id'    => $request->get('company_id'),
            'company_name'    => $request->get('company_name'),
            'company_rate'    => $request->get('company_rate'),
        ];

        \Mail::send('mail.get-quotation', ['data' => $data],
            function ($message) use ($data)
            {
                $message
                    ->from($data['no-reply'])
                    ->to($data['admin'])->subject('Some body wrote to you online')
                    ->to($data['Email'])->subject('Your submitted information')
                    ->to(GET_QUOTATION_MAIL_ADDRESS, 'elbiheiry')->subject('Feedback');
            });

        //  Mail::send('emails.activation', $data, function($message){
        //     $message->from(env('MAIL_USERNAME'),'Test');
        //     $message->to($email)->subject($subject);
        // });

     $notification = array(
            'message' => 'Thanks! We shall get back to you soon.', 
            'alert-type' => 'success'
        );
        \Session::flash('notification',$notification);
      return back()->with('success','Mail Send successfull !');
  }
}
