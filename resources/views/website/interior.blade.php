@extends('website.master-layout')

@push('css')

@endpush

@section('content')

<div class=" overview-bgi d-print-none" style="background-image:url('../../assets/images/interior.jpg' );">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-pad">
            <!-- construction Form -->
           
               <div class="submit-address dashboard-list">
               
                  <div class="row">
                     <h3 class="text-center">Interior</h3>
                     <h5 style="padding-left: 30px;
                        color: white;">
                        Select the options given below 
                     </h5>
                  </div>
                  @include('website.includes.search_filter_form')
               </div>
          
         </div>
      </div>
   </div>
   <div class="clearfix visible-xs"></div>
</div>
<div class="row">

    
<body>
<div class="container">
  <div class="row">
  
  </div>
    
        <div class="row">
    
            <div class="col-md-12">
            
            
<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
         
        <thead>
            <tr>
              <th>subcribed_company</th>
              <th>plot_location</th>
                <th>covered_area</th>
              <th>rate_sqft</th>
              <th>total</th>
              <th>Details</th>
              <th>Get Quotation</th>
            </tr>
          </thead>
        

          <tbody>
         
            @foreach($cc as $index => $value)
           <tr>
              <td>{{ $value->company->c_name }}
              <a href="{{ route('company.detail',$value->company_id) }}">Profile </a></td>
              <td>{{$value->plot_location}}</td>
              <td>{{$value->covered_area}}</td>
              <td>{{$value->rate_sqft}}</td>
              <td>{{$value->total}}</td>
              <td>
                <button type="button" class="btn btn-warning" onclick="detail_get('{{ $value->id }}','{{ $value->subcribed_company }}', &quot;Zaheer A Shiekh and Associates  &quot;,'arc_res_','5m','0')">Detail</button>
              </td>
              <td>
                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="send_company_id('4474',&quot;Zaheer A Shiekh and Associates&quot;,'0')">Get Quotation</button>
              </td>
           </tr>
           @endforeach
          </tbody>
        </table>

  
  </div>
  </div>
</div>

</div>


@include('website.includes.conpany-detail') 
@include('website.includes.get-quotation')

@include('website.includes.google_recaptcha')

@endsection

@push('script')
  @include('website.includes.company-detail-and-get-quotation-script')
@endpush