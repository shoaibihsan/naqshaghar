@extends('website.master-layout')

@push('css')

@endpush

@section('content')

<div class="overview-bgi listing-banner term-banner-div" >
   <div class="container listing-banner-info">
      <div class="row">
         <div class="col-lg-12 col-md-12 clearfix">
            <div class="text">
               <!--<h1> Terms </h1>-->
            </div>
         </div>
      </div>
   </div>
</div>
<div class="listing-details-page content-area-6 pt-0">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <!-- listing description start -->
            <div class="listing-description mb-40" id="about_us_description">
               <p></p>
               <article id="post-4356" class="post-4356 page type-page status-publish has-post-thumbnail hentry">
                  <header class="entry-header">
                  </header>
                  <!-- .entry-header -->
                  <div class="entry-content">
                     <h3 class="text-center">
                           <span class="text-decorations"><strong>
                           TERMS &amp; CONDITIONS for Professionals</strong></span></h3>
                        <p>
                           <strong><br>
                        <a href="https://www.naqshaghar.com/">Naqshaghar.com</a> is just connecting building professionals with public to boost your business.</strong><strong>&nbsp;</strong><strong>We are establishing direct link between you and end user, therefore we are not responsible for any revenue generation</strong>
                     </p>
                     <p><strong>1.&nbsp; Our services after registration</strong></p>
                     <p>In registration, only company’s portfolio will be uploaded on the website. Our registration life is 10 years for each profession/category without displaying their rates &amp; contact details.</p>
                     <p><strong>2.&nbsp; Our services after subscription</strong></p>
                     <p>After registration client have to select one of the Yearly subscription package to display their Rates and Contact details. Client must provide complete details by sending us at <a href="mailto:info@naqshaghar.com"><strong>info@naqshaghar.com</strong></a></p>
                     <p><strong>3.&nbsp; Payment method</strong></p>
                     <p>Naqsha Ghar will only accept payment through cross cheques &amp; online banking in favor of NAQSHA GHARPvt (Ltd). Portfolio will be uploaded after receiving amount in “NAQSHA GHAR” account within three working days.</p>
                     <p><strong>&nbsp;Client will pay registration fee and subscription package for each city and category separately.</strong></p>
                     <p><strong>4.&nbsp; Project percentage</strong></p>
                     <p><strong>Naqsha Ghar</strong>&nbsp;will receive a predetermined percentage of project deal from the client other than the registration or subscription. If the client withholds or provide inaccurate information regarding the deal, <strong>Naqsha Ghar</strong>&nbsp;holds authority to remove client’s portfolio from website and will take legal action against company, registration will be cancelled and company’s name will be displayed in the blacklist.</p>
                     <p><strong>5.&nbsp; Company’s information</strong></p>
                     <p>We reserve the right to edit or remove profile content if any wrong/incomplete information is noticed. Client can change their portfolio, rates &amp; pictures by sending us request on letterhead or via approved email of your company.</p>
                     <p><strong>6.&nbsp; PUBLIC SELECTION</strong></p>
                     <p>QUOTATIONS GENERATED WILL BE TRANSFERRED TO THE SELECTED COMPANY. Public can also call directly to the professionals. We are not giving any commitment of confirmed clients/ projects because it depends on public choice to select.</p>
                     <p><strong>7.&nbsp; Follow ups</strong></p>
                     <p>Naqsha reserve the right to make follow ups once the meeting will be conducted.</p>
                     <p><strong>8.&nbsp; Refund Policy</strong></p>
                     <p>We do not have any refund policy.</p>
                     <p><strong>We shall not be responsible for any dispute, transactions or commitments between client and public. Above Mentioned terms and conditions shall be governed in accordance with the laws of Government of Pakistan.</strong></p>
                  </div>
                  <!-- .entry-content -->
               </article>
               <!-- #post-## -->
               <p></p>
            </div>
            
            <div class="col-lg-4 col-md-4">
            </div>
         </div>
      </div>
   </div>
</div>

@endsection

@push('script')

@endpush