@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	@include('website.includes.header_banners.aboutus-header')

	<div class="overview-bgi listing-banner" style="background-image:url('https://www.naqsha.com.pk/wp-content/themes/naqsha/img/cover/arch.jpg?v1.2 ');background-size: 100% 100%;">
    <div class="container listing-banner-info">
        <div class="row">
            <div class="col-lg-7 col-md-12 clearfix">
                <div class="text">
                    <h1> Commercial Interior </h1>
                   
                 
                </div>
            </div>
            <div class="col-lg-5 col-md-12 clearfix">
                <div class="cover-buttons">
                    <ul>
                        <!--<li><a href="messages.html">Add Review</a></li>
                        <li><a href="login.html">Get a Quote</a></li>-->
                        <!--<li><a href="#">Contact Us</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="listing-details-page content-area-6" style="padding-top:40px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <!-- listing description start -->
              


                
                <!-- gallery start -->
                <div class="gallery">
                    <h3 class="heading-2">
                    Gallery
                    </h3>
                    <div class="container">
                        <div class="row">
                                                             
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/11-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/11-2.jpg" alt="11" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/10-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/10-2.jpg" alt="10" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/9-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/9-2.jpg" alt="9" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/8-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/8-2.jpg" alt="8" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/7-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/7-2.jpg" alt="7" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/6-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/6-2.jpg" alt="6" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/5-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/5-2.jpg" alt="5" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/4-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/4-2.jpg" alt="4" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/3-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/3-2.jpg" alt="3" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/2-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/2-2.jpg" alt="2" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/23.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/23.jpg" alt="23" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/22.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/22.jpg" alt="22" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/21.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/21.jpg" alt="21" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/20.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/20.jpg" alt="20" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/19.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/19.jpg" alt="19" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/18.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/18.jpg" alt="18" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/17-1.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/17-1.jpg" alt="17" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/16-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/16-2.jpg" alt="16" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/15-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/15-2.jpg" alt="15" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/14-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/14-2.jpg" alt="14" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/13-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/13-2.jpg" alt="13" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/12-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/12-2.jpg" alt="12" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                           </div>
                           
                    </div>
                </div>
                <!-- Location start
                
                                
                            </div>
            <!-- Services -->
           
				</div>
            </div>
        </div>
    </div>

@endsection

@push('script')

@endpush