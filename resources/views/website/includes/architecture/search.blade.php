<div class=" overview-bgi d-print-none" style="background-image:url('https://www.naqsha.com.pk/wp-content/themes/naqsha/img/cover/arch.jpg ');">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-pad">
            <!-- Architecture Form -->
            <form method="post" id="architecture_form">
               <div class="submit-address dashboard-list">
                  
                  <div class="row">
                     <h3 class="text-center">Architecture</h3>
                     <h5 style="padding-left: 30px;
                        color: white;">
                        Select the options given below 
                     </h5>
                  </div>
                  <div class="contact-2">
                     <div class="row pad-2">
                        <div class="col-md-4 col-sm-12">
                           <div class="form-group css_custom_select">
                              <label>Professional Location</label>
                              <select class="form-control text-light bg-dark my_select2 city_area_from_database select2-hidden-accessible" name="professional_location" id="professional_location" data-select2-id="professional_location" tabindex="-1" aria-hidden="true">
                                 <option value="1470-1">Jinnah Colony, Faisalabad</option>
                                 <option value="1471-1">Batala Colony, Faisalabad</option>
                                 <option value="1476-2">DC Colony, Gujranwala</option>
                                 <option value="1477-2">Rahwali, Gujranwala</option>
                                 <option value="1488-2">Canal View, Gujranwala</option>
                                 <option value="1498-2">City Housing Society, Gujranwala</option>
                                 <option value="1336-3">Affandi colony, Islamabad</option>
                                 <option value="1346-3">Burma Town, Islamabad</option>
                                 <option value="1357-3">Jagoit, Islamabad</option>
                                 <option value="1374-3">Saddar, Islamabad</option>
                                 <option value="1394-3">Sector E-11, Islamabad</option>
                                 <option value="1406-3">Sector F-11, Islamabad</option>
                                 <option value="1419-3">Sector G-10, Islamabad</option>
                                 <option value="1432-3">Sector G-9, Islamabad</option>
                                 <option value="1469-3">Blue Area, Islamabad</option>
                                 <option value="1489-3">F-10, Islamabad</option>
                                 <option value="1490-3">Nazimuddin Road, Islamabad</option>
                                 <option value="1126-4">Abdullah Haroon Road, Karachi</option>
                                 <option value="1127-4">B.M.C.H.S, Karachi</option>
                                 <option value="1154-4">Nazimabad, Karachi</option>
                                 <option value="1157-4">Orangi Town, Karachi</option>
                                 <option value="1164-4">Shah Latif Town, Karachi</option>
                                 <option value="1166-4">Shikarpur colony, Karachi</option>
                                 <option value="1177-4">Federal B Area FB, Karachi</option>
                                 <option value="1179-4">Civil lines, Karachi</option>
                                 <option value="1180-4">Sharifabad, Karachi</option>
                                 <option value="1196-4">Gulshan-e-Maymar, Karachi</option>
                                 <option value="1214-4">Korangi, Karachi</option>
                                 <option value="1224-4">Khurrambadi, Karachi</option>
                                 <option value="1243-4">Babar town, Karachi</option>
                                 <option value="1271-4">Gulshan-e-Iqbal, Karachi</option>
                                 <option value="1279-4">Nipa society, Karachi</option>
                                 <option value="1290-4">Gulzar-e-Hijri, Karachi</option>
                                 <option value="1327-4">DHA Phase 2, Karachi</option>
                                 <option value="1329-4">DHA Phase 4, Karachi</option>
                                 <option value="1331-4">DHA Phase 6, Karachi</option>
                                 <option value="1499-4">Essa Nagri, Karachi</option>
                                 <option value="1500-4">North Karachi, Karachi</option>
                                 <option value="1501-4">Akhtar Colony, Karachi</option>
                                 <option value="983-5" selected="">Abpara Housing Soceity, Lahore</option>
                                 <option value="989-5" selected="">Allama Iqbal Town , Lahore</option>
                                 <option value="994-5" selected="">Bahria Town, Lahore</option>
                                 <option value="999-5" selected="">Bedian Road, Lahore</option>
                                 <option value="1004-5" selected="">Burki Road, Lahore</option>
                                 <option value="1006-5" selected="">Canal Gardens , Lahore</option>
                                 <option value="1008-5" selected="">Cantt Askari 1, Lahore</option>
                                 <option value="1020-5" selected="">DHA, Lahore</option>
                                 <option value="1021-5" selected="">DHA Phase 1, Lahore</option>
                                 <option value="1022-5" selected="">DHA Phase 2, Lahore</option>
                                 <option value="1023-5" selected="">DHA Phase 3, Lahore</option>
                                 <option value="1024-5" selected="">DHA Phase 4, Lahore</option>
                                 <option value="1025-5" selected="">DHA Phase 5, Lahore</option>
                                 <option value="1026-5" selected="">DHA Phase 6, Lahore</option>
                                 <option value="1027-5" selected="">DHA Rahbar, Lahore</option>
                                 <option value="1028-5" selected="">DHA(EME), Lahore</option>
                                 <option value="1037-5" selected="">Faisal Town, Lahore</option>
                                 <option value="1038-5" selected="">Feroze Wala, Lahore</option>
                                 <option value="1040-5" selected="">Garden Town, Lahore</option>
                                 <option value="1050-5" selected="">Gulberg  1, Lahore</option>
                                 <option value="1051-5" selected="">Gulberg  2, Lahore</option>
                                 <option value="1052-5" selected="">Gulberg  3, Lahore</option>
                                 <option value="1054-5" selected="">Gulshan-e-Ravi, Lahore</option>
                                 <option value="1059-5" selected="">Ichra, Lahore</option>
                                 <option value="1060-5" selected="">Iqbal Avenue, Lahore</option>
                                 <option value="1061-5" selected="">Izmir Town, Lahore</option>
                                 <option value="1064-5" selected="">Johar Town, Lahore</option>
                                 <option value="1065-5" selected="">Jubilee Town, Lahore</option>
                                 <option value="1067-5" selected="">Kahna , Lahore</option>
                                 <option value="1069-5" selected="">Khayaban-e-Amin, Lahore</option>
                                 <option value="1073-5" selected="">LDA Avenue, Lahore</option>
                                 <option value="1076-5" selected="">Model Town, Lahore</option>
                                 <option value="1080-5" selected="">Muslim Town, Lahore</option>
                                 <option value="1082-5" selected="">Nawab Town, Lahore</option>
                                 <option value="1085-5" selected="">New Garden Town, Lahore</option>
                                 <option value="1090-5" selected="">Paragon City, Lahore</option>
                                 <option value="1091-5" selected="">Park View , Lahore</option>
                                 <option value="1094-5" selected="">Rail Town, Lahore</option>
                                 <option value="1095-5" selected="">Raiwind, Lahore</option>
                                 <option value="1097-5" selected="">Saeed Park, Lahore</option>
                                 <option value="1103-5" selected="">Shahdara, Lahore</option>
                                 <option value="1105-5" selected="">Shahjamal, Lahore</option>
                                 <option value="1113-5" selected="">Super Town , Lahore</option>
                                 <option value="1115-5" selected="">Township, Lahore</option>
                                 <option value="1118-5" selected="">Valencia , Lahore</option>
                                 <option value="1119-5" selected="">Wahdat Colony, Lahore</option>
                                 <option value="1120-5" selected="">Wapda Town , Lahore</option>
                                 <option value="1460-5" selected="">Kalma Chowk, Lahore</option>
                                 <option value="1463-5" selected="">Sabzazar, Lahore</option>
                                 <option value="1467-5" selected="">DHA Phase 8, Lahore</option>
                                 <option value="1468-5" selected="">Sarwar Road, Lahore</option>
                                 <option value="1475-5" selected="">Karim BLock, Lahore</option>
                                 <option value="1480-5" selected="">Iqbal Town, Lahore</option>
                                 <option value="1483-5" selected="">Cantt, Lahore</option>
                                 <option value="1485-5" selected="">Gulberg 5, Lahore</option>
                                 <option value="1486-5" selected="">Airline Society, Lahore</option>
                                 <option value="1491-5" selected="">GT Road, Lahore</option>
                                 <option value="1492-5" selected="">Shalamar Link Road, Lahore</option>
                                 <option value="1497-5" selected="" data-select2-id="5">Quaid-e-Azam Industrial Estate, Lahore</option>
                                 <option value="1465-6">Royal Orchard, Multan</option>
                                 <option value="1466-6">Shah Rukn-e-Alam Colony, Multan</option>
                                 <option value="1479-6">Model Town, Multan</option>
                                 <option value="1482-6">Chowk Shaheedan, Multan</option>
                                 <option value="1472-7">Liaqat Road, Rawalpindi</option>
                                 <option value="1487-8">Neka Pura, Sialkot</option>
                                 <option value="1496-9">Gulshan-e-Iqbal, Rahim Yar Khan</option>
                                 <option value="1502-10">Cantt, Peshawar</option>
                                 <option value="1503-11">Jada, Jhelum</option>
                                 <option value="1504-12">Gulshan Colony, Jhang</option>
                              </select>
                              
                           </div>
                           <div class="form-group css_custom_select">
                              <label>Plot Location </label>
                              <select class="form-control text-light bg-dark my_select2 city_area_from_database select2-hidden-accessible" name="plot_location" id="plot_location" data-select2-id="plot_location" tabindex="-1" aria-hidden="true">
                                 <option value="1470-1">Jinnah Colony, Faisalabad</option>
                                 <option value="1471-1">Batala Colony, Faisalabad</option>
                                 <option value="1476-2">DC Colony, Gujranwala</option>
                                 <option value="1477-2">Rahwali, Gujranwala</option>
                                 <option value="1488-2">Canal View, Gujranwala</option>
                                 <option value="1498-2">City Housing Society, Gujranwala</option>
                                 <option value="1336-3">Affandi colony, Islamabad</option>
                                 <option value="1346-3">Burma Town, Islamabad</option>
                                 <option value="1357-3">Jagoit, Islamabad</option>
                                 <option value="1374-3">Saddar, Islamabad</option>
                                 <option value="1394-3">Sector E-11, Islamabad</option>
                                 <option value="1406-3">Sector F-11, Islamabad</option>
                                 <option value="1419-3">Sector G-10, Islamabad</option>
                                 <option value="1432-3">Sector G-9, Islamabad</option>
                                 <option value="1469-3">Blue Area, Islamabad</option>
                                 <option value="1489-3">F-10, Islamabad</option>
                                 <option value="1490-3">Nazimuddin Road, Islamabad</option>
                                 <option value="1126-4">Abdullah Haroon Road, Karachi</option>
                                 <option value="1127-4">B.M.C.H.S, Karachi</option>
                                 <option value="1154-4">Nazimabad, Karachi</option>
                                 <option value="1157-4">Orangi Town, Karachi</option>
                                 <option value="1164-4">Shah Latif Town, Karachi</option>
                                 <option value="1166-4">Shikarpur colony, Karachi</option>
                                 <option value="1177-4">Federal B Area FB, Karachi</option>
                                 <option value="1179-4">Civil lines, Karachi</option>
                                 <option value="1180-4">Sharifabad, Karachi</option>
                                 <option value="1196-4">Gulshan-e-Maymar, Karachi</option>
                                 <option value="1214-4">Korangi, Karachi</option>
                                 <option value="1224-4">Khurrambadi, Karachi</option>
                                 <option value="1243-4">Babar town, Karachi</option>
                                 <option value="1271-4">Gulshan-e-Iqbal, Karachi</option>
                                 <option value="1279-4">Nipa society, Karachi</option>
                                 <option value="1290-4">Gulzar-e-Hijri, Karachi</option>
                                 <option value="1327-4">DHA Phase 2, Karachi</option>
                                 <option value="1329-4">DHA Phase 4, Karachi</option>
                                 <option value="1331-4">DHA Phase 6, Karachi</option>
                                 <option value="1499-4">Essa Nagri, Karachi</option>
                                 <option value="1500-4">North Karachi, Karachi</option>
                                 <option value="1501-4">Akhtar Colony, Karachi</option>
                                 <option value="983-5" selected="">Abpara Housing Soceity, Lahore</option>
                                 <option value="989-5" selected="">Allama Iqbal Town , Lahore</option>
                                 <option value="994-5" selected="">Bahria Town, Lahore</option>
                                 <option value="999-5" selected="">Bedian Road, Lahore</option>
                                 <option value="1004-5" selected="">Burki Road, Lahore</option>
                                 <option value="1006-5" selected="">Canal Gardens , Lahore</option>
                                 <option value="1008-5" selected="">Cantt Askari 1, Lahore</option>
                                 <option value="1020-5" selected="">DHA, Lahore</option>
                                 <option value="1021-5" selected="">DHA Phase 1, Lahore</option>
                                 <option value="1022-5" selected="">DHA Phase 2, Lahore</option>
                                 <option value="1023-5" selected="">DHA Phase 3, Lahore</option>
                                 <option value="1024-5" selected="">DHA Phase 4, Lahore</option>
                                 <option value="1025-5" selected="">DHA Phase 5, Lahore</option>
                                 <option value="1026-5" selected="">DHA Phase 6, Lahore</option>
                                 <option value="1027-5" selected="">DHA Rahbar, Lahore</option>
                                 <option value="1028-5" selected="">DHA(EME), Lahore</option>
                                 <option value="1037-5" selected="">Faisal Town, Lahore</option>
                                 <option value="1038-5" selected="">Feroze Wala, Lahore</option>
                                 <option value="1040-5" selected="">Garden Town, Lahore</option>
                                 <option value="1050-5" selected="">Gulberg  1, Lahore</option>
                                 <option value="1051-5" selected="">Gulberg  2, Lahore</option>
                                 <option value="1052-5" selected="">Gulberg  3, Lahore</option>
                                 <option value="1054-5" selected="">Gulshan-e-Ravi, Lahore</option>
                                 <option value="1059-5" selected="">Ichra, Lahore</option>
                                 <option value="1060-5" selected="">Iqbal Avenue, Lahore</option>
                                 <option value="1061-5" selected="">Izmir Town, Lahore</option>
                                 <option value="1064-5" selected="">Johar Town, Lahore</option>
                                 <option value="1065-5" selected="">Jubilee Town, Lahore</option>
                                 <option value="1067-5" selected="">Kahna , Lahore</option>
                                 <option value="1069-5" selected="">Khayaban-e-Amin, Lahore</option>
                                 <option value="1073-5" selected="">LDA Avenue, Lahore</option>
                                 <option value="1076-5" selected="">Model Town, Lahore</option>
                                 <option value="1080-5" selected="">Muslim Town, Lahore</option>
                                 <option value="1082-5" selected="">Nawab Town, Lahore</option>
                                 <option value="1085-5" selected="">New Garden Town, Lahore</option>
                                 <option value="1090-5" selected="">Paragon City, Lahore</option>
                                 <option value="1091-5" selected="">Park View , Lahore</option>
                                 <option value="1094-5" selected="">Rail Town, Lahore</option>
                                 <option value="1095-5" selected="">Raiwind, Lahore</option>
                                 <option value="1097-5" selected="">Saeed Park, Lahore</option>
                                 <option value="1103-5" selected="">Shahdara, Lahore</option>
                                 <option value="1105-5" selected="">Shahjamal, Lahore</option>
                                 <option value="1113-5" selected="">Super Town , Lahore</option>
                                 <option value="1115-5" selected="">Township, Lahore</option>
                                 <option value="1118-5" selected="">Valencia , Lahore</option>
                                 <option value="1119-5" selected="">Wahdat Colony, Lahore</option>
                                 <option value="1120-5" selected="">Wapda Town , Lahore</option>
                                 <option value="1460-5" selected="">Kalma Chowk, Lahore</option>
                                 <option value="1463-5" selected="">Sabzazar, Lahore</option>
                                 <option value="1467-5" selected="">DHA Phase 8, Lahore</option>
                                 <option value="1468-5" selected="">Sarwar Road, Lahore</option>
                                 <option value="1475-5" selected="">Karim BLock, Lahore</option>
                                 <option value="1480-5" selected="">Iqbal Town, Lahore</option>
                                 <option value="1483-5" selected="">Cantt, Lahore</option>
                                 <option value="1485-5" selected="">Gulberg 5, Lahore</option>
                                 <option value="1486-5" selected="">Airline Society, Lahore</option>
                                 <option value="1491-5" selected="">GT Road, Lahore</option>
                                 <option value="1492-5" selected="">Shalamar Link Road, Lahore</option>
                                 <option value="1497-5" selected="" data-select2-id="6">Quaid-e-Azam Industrial Estate, Lahore</option>
                                 <option value="1465-6">Royal Orchard, Multan</option>
                                 <option value="1466-6">Shah Rukn-e-Alam Colony, Multan</option>
                                 <option value="1479-6">Model Town, Multan</option>
                                 <option value="1482-6">Chowk Shaheedan, Multan</option>
                                 <option value="1472-7">Liaqat Road, Rawalpindi</option>
                                 <option value="1487-8">Neka Pura, Sialkot</option>
                                 <option value="1496-9">Gulshan-e-Iqbal, Rahim Yar Khan</option>
                                 <option value="1502-10">Cantt, Peshawar</option>
                                 <option value="1503-11">Jada, Jhelum</option>
                                 <option value="1504-12">Gulshan Colony, Jhang</option>
                              </select>
                            
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                           <div class="form-group">
                              <label>Type</label>
                              <select class="form-control text-light bg-dark" id="project_type" name="porperty">
                                 <option value="arc_res_">Residential</option>
                                 <option value="arc_comm_">Commercial</option>
                              </select>
                           </div>
                           <div class="form-group">
                              <label>Plot Size</label>
                              <select class="form-control text-light bg-dark" id="plot_size" name="size">
                                 undefined
                                 <option value="5m">5 Marla</option>
                                 <option value="10m">10 Marla</option>
                                 <option value="1k">1 kanal</option>
                                 <option value="2k">2 Kanal</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                           <div class="form-group" id="floor_main_div">
                              <label>No. of Floors</label>
                              <select class="form-control text-light bg-dark" name="construction_type" id="construction_type">
                                 <option value="B">2 Floor + Basement</option>
                              </select>
                           </div>
                           <div class="form-group mb-0" style="margin-top:50px;">
                              <button id="btnSearch" class="btn btn-primary" type="button" style="width:100%;">Search</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
            <style>
               .select2 {
               width: 100% !important;
               }
            </style>
         </div>
      </div>
   </div>
   <div class="clearfix visible-xs"></div>
</div>