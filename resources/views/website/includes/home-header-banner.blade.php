<div class=banner id=banner>
         <div class="carousel slide"id=bannerCarousole data-ride=carousel>
            <div class=carousel-inner>
               <div class="carousel-item active banner-max-height" id="bg_img">
                  <img class="d-block w-100 h-100 img-responsive" style="animation: none;" src='public/assets/images/mainpage_header.jpg'>
                  <!-- <img data-aos="fade-up"
                     data-aos-duration="3000" class="scale-up-left d-block img-2 fade-up-image" src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/jawad-bashir.webp?v1.2" > -->
                  <div class="text-center banner-slider-inner carousel-caption d-flex"></div>
               </div>
            </div>
         </div>
         <div class="bi-4 row">
      
            <div class="col-md-12 col-xl-12 col-lg-12 col-xs-12 col-sm-12 mb_padding">
               <div class="text-center1" style="margin-top: 105px !important;">
                 <!--<img id="naqsha_pvt_ltd" class="logo-responsive img-responsive d-lg-block d-md-none d-none"src="public/assets/images/Banner_logo_final.png" alt="logo" style="display: none !important;>!--> 
                 
                  <h1 class="scale-up-ver-center mb-1 mt-3" class=b-text>
                     Select The Perfect Constructional <br>
                     Services For Your Buildings
                  </h1>
                  <div class=banner-info>
                     <ul id="icon_ul" style="">
                        <li class="" >
                           <a href="architecture" class="search-below-buttons">
                              <div class=""></div>
                              <h5>Architecture</h5>
                           </a>
                        </li>
                        <li >
                           <a href="construction" class="search-below-buttons">
                              <div class=""></div>
                              <h5>Construction</h5>
                           </a>
                        </li>
                        <li >
                           <a href="interior" class="search-below-buttons">
                              <div class=""></div>
                              <h5>Interior</h5>
                           </a>
                        </li>
                        <li >
                           <a href="landscape" class="search-below-buttons">
                              <div class=""></div>
                              <h5>Landscape</h5>
                           </a>
                        </li>
                        <li >
                           <a href="town-planner" class="search-below-buttons">
                              <div class=""></div>
                              <h5>Town Planning</h5>
                           </a>
                        </li>
                        <li >
                           <a href="engineers" class="search-below-buttons">
                              <div class=""></div>
                              <h5>Engineering</h5>
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
               <!-- Registered Companies Start -->
               
               <div class="row d-none d-xl-block">
                  <div class="jumbotron" data-aos="fade-left"
                     data-aos-duration="3000">
                     <h6 class="pb-3">Registered Companies</h6>
                     <ul class="pt-2 pl-2 pr-2" style="">
                        <a href="#">
                           <li class="pb-2">  
                              Architecture  
                              <span class="arch_count_id">({{ $data['ar'] }})</span>
                           </li>
                        </a>
                        <a href="#">
                           <li class="pb-2"> 
                              Construction 
                              <span class="con_count_id">({{ $data['con'] }})</span>
                           </li>
                        </a>
                        <a href="">
                           <li class="pb-2"> 
                              Interior 
                              <span class="inter_count_id">({{ $data['in'] }})</span>
                           </li>
                        </a>
                        <a href="">
                           <li class="pb-2"> 
                              Landscape <span class="land_count_id">({{ $data['land'] }})</span>
                           </li>
                        </a>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Registered Companies END -->