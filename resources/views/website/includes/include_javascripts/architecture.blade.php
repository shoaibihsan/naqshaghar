<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js

"></script>
<script type="text/javascript">
  function recaptchaCallback() {
 jQuery('#table_quote').show();
 $('#submit_quote').removeAttr('disabled');
}
var company_name, company_rate, company_id, message = jQuery('.contact__msg');
var get_all_architect = true;
jQuery(document).ready(function($) {
 jQuery('#print_quote').click(function($) {
  if (jQuery('#email').val() != "" && jQuery('#f_name').val() != "" && jQuery('#l_name').val() != "" && jQuery('#mobile').val() != "") {
   window.print();
  } else {
   message.fadeIn()
   message.empty()
   message.html('<p>Do Not Leave The fields Empty</p>');
   setTimeout(function() {
    message.fadeOut();
   }, 5000);


  }
 });
 // Your code in here
 var get_area = '';
 var get_city = '';
 // var get_area = '1040';
 //var get_city = '5';
 var area_id, select_city_id, my_api_data, my_city_area_data;
 var where_city_area_class = ".city_area_from_database";
 var where_city_id = "#city_from_database";
 var where_area_id = "#area_from_database";

 var where_area_professional_id = "#area_of_professional";
 var construction_type = "#construction_type";


 jQuery('.my_select2').select2();



 var api_url = "https://www.naqsha.com.pk/api-call/db/query/city_area_api.php?city_area_from_database=1";
 jQuery.ajax({
  type: "GET",
  dataType: "json",
  url: api_url,


  success: function(response) {
   //  console.log(response);

   my_city_area_data = response;
   //   console.log(my_api_data);
   content = '';
   for (var x = 0; x < response.all_city.length; x++) {
    for (var i = 0; i < response.all_area.length; i++) {
     if (response.all_city[x].city_id == response.all_area[i].city_id) {
      content += '<option  value="' + response.all_area[i].area_id + '-' + response.all_city[x].city_id;
      content += '"';
      if (response.all_city[x].city_name == "Lahore") {
       content += 'selected';
      }
      content += '>';
      content += response.all_area[i].area_name + ', ' + response.all_city[x].city_name;;
      content += "</option >";
     }
    }

   }
   // console.log(content);
   jQuery(where_city_area_class).empty();
   jQuery(where_city_area_class).append(content);

   //  $("#btnSearch").trigger("click");


  }



 });

 var table = jQuery('#example').DataTable({
  responsive: true,
  "order": [],
  pageLength: 50,
  columnDefs: [{
    responsivePriority: 1,
    targets: 0
   },
   {
    responsivePriority: 2,
    targets: 1
   },
   {
    responsivePriority: 4,
    targets: 2
   },
   {
    responsivePriority: 5,
    targets: 3
   },
   {
    responsivePriority: 6,
    targets: 4
   },
   {
    responsivePriority: 3,
    targets: 5
   },
   {
    responsivePriority: 7,
    targets: 6
   },
   {
    responsivePriority: 8,
    targets: 7
   },
   {
    orderable: false,
    targets: -1
   },
   {
    orderable: false,
    targets: 2
   },
   {
    orderable: false,
    targets: 3
   },
   {
    orderable: false,
    targets: 4
   },
   {
    orderable: false,
    targets: 6
   }
  ],
  bAutoWidth: false,
  aoColumns: [{
    sWidth: '2%'
   },
   {
    sWidth: '38%'
   },
   {
    sWidth: '24%'
   },
   {
    sWidth: '8%'
   },
   {
    sWidth: '10%'
   },
   {
    sWidth: '8%'
   },
   {
    sWidth: '10%'
   }
  ]
 
 });



 var post_url_query = post_url;

 $("#btnSearch").click(function(event) {
  $("#btnSearch").toggle();
  // $(this).hide();
  table.clear().draw();
  $(".page_loader").fadeIn("fast");
  var form_data = arch_form.serialize(); //Encode form elements for submission


  jQuery.ajax({
   url: post_url_query,
   type: "POST",
   dataType: "json",
   data: form_data,
   success: function(data) {
    var temp = data;
    console.log(data);
    for (var i = 0; i < data.data.length; i++) {
     var b = i;
     if (data.data[i].top_10 == 1) {
      var top_10_arch = table.row.add([
        ++b,

        '<h6>' + data.data[i].title + ' <span class="badge badge-secondary" data-toggle="tooltip" data-placement="top" title="Premium Plus Companies">Premium Plus Companies<i class="fa fa-star" aria-hidden="true" ></i></span> ' + '</h6><small>' + data.data[i].area.trim() + ', ' + data.data[i].city + '</small><a class="btn btn-primary" style="font-size: 12px;color: black;float: left;padding: 0px 5px !important;border-radius: 0.40rem !important;" href="https://www.naqsha.com.pk/' + data.data[i].url + '?cat=architecture" target="_blank">profile <i class="fa fa-external-link" aria-hidden="true" style="font-size: 12px;"></i></a>',
        $('#plot_location option:selected').text(),

        //$('#design_requirment option:selected').text(),
        $('#project_type option:selected').text(),
        $('#plot_size option:selected').text(),

        'Rs. ' + data.data[i].rate,

        '<button type="button" class="btn btn-warning"  onclick="detail_get(&#39;' + data.data[i].id + '&#39;,&#39;' + data.data[i].url + '&#39;, &quot;' + data.data[i].title + '  &quot;,&#39;' + jQuery('#project_type').val() + '&#39;,&#39;' + jQuery('#plot_size').val() + '&#39;,&#39;' + data.data[i].rate + '&#39;)">Detail</button>',
        '<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="send_company_id(&#39;' + data.data[i].id + '&#39;,&quot;' + data.data[i].title + '&quot;,&#39;' + data.data[i].rate + '&#39;)">Get Quotation</button>'
       ]).draw()
       .node();
      $(top_10_arch)
       .addClass('TOP_10');
     } else {
      table.row.add([
       ++b,

       '<h6>' + data.data[i].title + '</h6><small>' + data.data[i].area.trim() + ', ' + data.data[i].city + '</small><a class="btn btn-primary" style="font-size: 12px;color: black;float: left;padding: 0px 5px !important;border-radius: 0.40rem !important;" href="https://www.naqsha.com.pk/' + data.data[i].url + '?cat=architecture" target="_blank">profile <i class="fa fa-external-link" aria-hidden="true" style="font-size: 12px;"></i></a>',
       $('#plot_location option:selected').text(),

       //$('#design_requirment option:selected').text(),
       $('#project_type option:selected').text(),
       $('#plot_size option:selected').text(),

       'Rs. ' + data.data[i].rate,

       '<button type="button" class="btn btn-warning"  onclick="detail_get(&#39;' + data.data[i].id + '&#39;,&#39;' + data.data[i].url + '&#39;, &quot;' + data.data[i].title + '  &quot;,&#39;' + jQuery('#project_type').val() + '&#39;,&#39;' + jQuery('#plot_size').val() + '&#39;,&#39;' + data.data[i].rate + '&#39;)">Detail</button>',
       '<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="send_company_id(&#39;' + data.data[i].id + '&#39;,&quot;' + data.data[i].title + '&quot;,&#39;' + data.data[i].rate + '&#39;)">Get Quotation</button>'
      ]).draw(false);
     }

    }
    $("#btnSearch").toggle();
    jQuery(".page_loader").fadeOut("fast");
   },
   error: function(data) {
    console.log(data);
    $("#btnSearch").toggle();
    jQuery(".page_loader").fadeOut("fast");
   }

  });

 });

 var quote_form = jQuery('#submit_quote');

 var send_mail_api = "https://www.naqsha.com.pk/api-call/send_email/send_mail.php";

 quote_form.click(function() {

  var quote_data = jQuery('#get_quote').serializeArray();

 });

});

function send_company_id(id, name, rate) {
 jQuery(".page_loader").fadeIn("fast");
 company_name = name;
 company_id = id;
 company_rate = rate;

 jQuery('#table_quote').empty();
 jQuery('#table_quote').hide();
 jQuery('#table_quote').append('<tr style=" background-color: rgba(0,0,0,.05);"> <th>Company Name</th> <td>' + company_name + '</td></tr><tr> <th>Plot Location</th> <td>' + jQuery('#plot_location option:selected').text() + '</td></tr><tr style=" background-color: rgba(0,0,0,.05);"> <th>Professional Lahore</th> <td>' + jQuery('#professional_location option:selected').text() + '</td></tr><tr> <th>Type</th> <td>' + jQuery('#project_type option:selected').text() + '</td></tr><tr style=" background-color: rgba(0,0,0,.05);"> <th>Plot Size</th> <td>' + jQuery('#plot_size option:selected').text() + '</td></tr><tr> <th>Rate</th> <td> Rs. ' + company_rate + '</td></tr>');
 jQuery(".page_loader").fadeOut("fast");
}

function detail_get(id, url, name, type, size, rate) {
 var floor = '';
 $('#myModaldetail').modal('show');
 floor = jQuery('#construction_type').val();
 jQuery(".page_loader").fadeIn("fast");
 jQuery('#detail_table_view').empty();
 jQuery('#detail_plot_type').empty();
 jQuery('#arc_res_DS').empty();
 jQuery('#view_profile_company').empty();
 jQuery('#company_name_detail').empty();
 jQuery('#company_name_detail').append(name);
 jQuery('#detail_plot_type').append(jQuery('#plot_size option:selected').text() + ' ' + jQuery('#project_type option:selected').text());
 if (jQuery('#project_type').val() == 'arc_res_') {

  jQuery('#detail_table_view').append('<tr style=" background-color: rgba(0,0,0,.05);"> <th>Type</th> <th> ' + jQuery('#construction_type option:selected').text() + ' </th></tr><tr> <th>Package Fee</th> <td> Rs. ' + rate + '</td></tr>');
  jQuery('#arc_res_DS').append('<strong>List of Basic Drawing Set</strong><ol><li>Planning and designing (design concept).<li>Front elevation design (3D presentation).<li>Submission drawings with 5 blue prints for approval.<li>Foundation working.<li>Working plans.<li>Selection details.<li>Elevation working.<li>Electric layouts.<li>Sanitary & sewerage layouts.<li>Structural layouts.<li>Roof plan.</ol>');
  jQuery('#view_profile_company').attr("href", 'https://www.naqsha.com.pk/services/' + url);
 } else {

  jQuery('#detail_table_view').append('<tr style=" background-color: rgba(0,0,0,.05);"> <th>Type</th> <th> ' + jQuery('#construction_type option:selected').text() + ' </th></tr><tr> <th>Package Fee</th> <td> Rs. ' + rate + '</td></tr>');
  jQuery('#arc_res_DS').append('<strong>List of Basic Drawing Set</strong><ol><li>Planning and designing (design concept).<li>Front elevation design (3D presentation).<li>Submission drawings with 5 blue prints for approval.<li>Foundation working.<li>Working plans.<li>Selection details.<li>Elevation working.<li>Electric layouts.<li>Sanitary & sewerage layouts.<li>Structural layouts.<li>Roof plan.</ol>');
  jQuery('#view_profile_company').attr("href", 'https://www.naqsha.com.pk/services/' + url);
 }
}


function send_quotation(id, name, rate){
    company_name = name;
    company_id = id;
    company_rate = rate;
    $("#company_id").val(company_id);
    $("#company_name").val(company_name);
    $("#company_rate").val(company_rate);
    $("#myModal").modal('show');
}
        
</script>