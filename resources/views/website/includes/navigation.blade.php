<header class="main-header header-transparent sticky-header" id="head">
         <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark">
               <a class="navbar-brand logo" href="{{ route('home.index') }}">
                  <img src="{{ asset('public/assets/images/Header_logo.png') }}" alt="logo">
               </a>
               <button class="navbar-toggler" id="sidebarCollapse" type="button"  aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div id="get_quotation" class="modal modal-xl" role="dialog">
                  <div class="modal-dialog modal-dialog-box">
                     <div class="modal-content">
                        <div class="modal-body text-color-white p-0">
                           <i class="fa fa-window-close fa-2x home-page-fa-window-close" aria-hidden="true"  data-dismiss="modal"></i>
                           <img src="{{ asset('public/assets/images/header.jpg') }}" width="100%" style="height: 100%;">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav header-ml ml-auto ">
                     <li class="nav-item dropdown">
                        <a class="nav-link"  href="#" data-toggle="modal" data-target="#get_quotation"  aria-haspopup="true" aria-expanded="false">
                        How to Get Quotation
                        </a>
                     </li>
                     <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Services 
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                           <li>
                              <a class="dropdown-item" href="architecture">
                                 Architecture
                              </a>
                           </li>
                           <li>
                              <a class="dropdown-item" href="construction">
                                 Construction
                              </a>
                           </li>
                           <li>
                              <a class="dropdown-item" href="interior">
                                 Interior
                              </a>
                           </li>
                           <li>
                              <a class="dropdown-item" href="landscape">
                                 Landscape
                              </a>
                           </li>
                           <li>
                              <a class="dropdown-item" href="town-planner">
                                 Town Planning
                              </a>
                           </li>
                         
                        </ul>
                     </li>
                     <li class="nav-item dropdown">
                        <a class="nav-link" href="about-us">
                        About us
                        </a>
                     </li>
                     <li class="nav-item dropdown">
                        <a class="nav-link" href="term-condition">
                        Terms & Conditions
                        </a>
                     </li>
                     <li class="nav-item dropdown">
                        <a class="nav-link" href="faq">
                        FAQs
                        </a>
                     </li>
                  <!--    <li class="nav-item dropdown">
                        <a class="nav-link" href="blogs">
                        Blogs    
                        </a>                                           
                     </li> -->
                     <li class="nav-item dropdown">
                        <a class="nav-link" id='btn-contact-us' >
                        Contact Us
                        </a>
                     </li>
                     <li class="nav-item dropdown">
                         <a href="#"><button class=" "style="margin-top:20px;color:#ffffff;width: 110px;border: 1px solid;background: transparent;font-weight: 700;height: 35px;font-size: 15px;
">For Business</button> </a>
                         
                     </li>
                  </ul>
               </div>
            </nav>
            <nav class="sidenav"></nav>
            <!-- Sidebar  -->
            <nav id="sidebar" class=" d-lg-none ">
               <div id="dismiss">
                  <i class="fa fa-angle-double-right"></i>
               </div>
               <div class="sidebar-header">
                  <h3>&nbsp;</h3>
               </div>
               <ul class="list-unstyled components">
                  <!--  <li class="active">
                     </li> -->
                  <li>
                     <a href='#' data-toggle="modal" data-target="#get_quotation"> How to Get Quotation</a>
                  </li>
                  <li>
                     <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Services</a>
                     <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                           <a  href="architecture">
                              Architecture
                           </a>
                        </li>
                        <li>
                           <a  href="construction">
                              Construction
                           </a>
                        </li>
                        <li>
                           <a  href="interior">
                              Interior
                           </a>
                        </li>
                        <li>
                           <a  href="landscape">
                              Landscape
                           </a>
                        </li>
                        <li>
                           <a  href="town-planner">
                              Town Planning
                           </a>
                        </li>
                        <li>
                           <a  href="engineers">
                              Engineering
                           </a>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <a class="nav-link" href="/about-us">
                        About us
                     </a>
                  </li>
                  <li>
                     <a class="nav-link" href="/term-condition">
                     Terms & Conditions
                     </a>
                  </li>
                  <li>
                     <a class="nav-link" href="faq">
                        FAQs
                     </a>
                  </li>
                  <li>
                     <a class="nav-link" href="/blogs">
                        Blogs
                     </a>
                  </li>
                  <li>
                     <a id='btn-contact-us-mobi'>
                        Contact Us
                     </a>
                  </li>
                  <li class="nav-item dropdown">
                     <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false">
                        Registered Companies
                     </a>
                     <ul class="collapse list-unstyled" id="pageSubmenu1">
                        <li>
                           <a  href="#">
                              Architecture 
                              <span class="arch_count_id"> </span>
                           </a>
                        </li>
                        <li>
                           <a  href="#">
                              Construction 
                              <span class="con_count_id"></span>
                           </a>
                        </li>
                        <li>
                           <a  href="#">
                              Interior 
                              <span class="inter_count_id"></span>
                           </a>
                        </li>
                        <li>
                           <a  href="#">
                              Landscape 
                              <span class="land_count_id"></span>
                           </a>
                        </li>
                     </ul>
                  </li>
               </ul>
            </nav>
         </div>
         <div class="overlay"></div>
      </header>