@extends('website.master-layout') 
@push('css') 
@endpush 
@section('content')

<div class=" overview-bgi d-print-none" style="background-image:url(' ../../assets/images/architecture-design.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-pad">
                <!-- construction Form -->
                <div class="submit-address dashboard-list">
                    
                    <div class="row">
                         <h3 class="text-center">Architecture</h3>
                         <h5 style="padding-left: 30px;
                            color: white;">
                            Select the options given below 
                         </h5>
                      </div>
                      @include('website.includes.search_filter_form')

                </div>

            </div>
        </div>
    </div>
    <div class="clearfix visible-xs"></div>
</div>
<div class="row">

    <body>
        <div class="container">
            <div class="row">
                <h2 class="text-center">Bootstrap styling for Datatable</h2>
            </div>

            <div class="row">

                <div class="col-md-12">

                    <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Subcribed Companies</th>
                                <th>Plot Location</th>
                                <th>Type</th>
                                <th>Plot size</th>
                                <th>Rates</th>
                                <th>Detail</th>
                                <th>Get Quotation</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach($cc as $index => $value)
                            <tr>

                                <td>
                                    {{ $value->company->c_name }}
                                    
                                    <a  target="_blank" href="{{ route('company.detail',$value->company_id) }}">
                                        <i class="fa fa-external-link" aria-hidden="true" style="font-size: 12px;"></i>

                                    Profile </a>
                                </td>
                                <td>{{$value->plot_location}}</td>
                                <td>{{$value->plot_type}}</td>
                                 <td>{{$value->plot_size}}</td>
                                <td>{{$value->rate}}</td>
                                <td>
                                    <button type="button" class="btn btn-warning" onclick="detail_get('{{ $value->id }}','{{ $value->subcribed_company }}','{{ $value->company['c_name'] }}','arc_res_','5m','{{ $value->rate }}')">Detail</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning"  onclick="send_quotation('{{ $value->company_id }}','{{ $value->company->c_name }}','{{ $value->rate }}')">Get Quotation</button>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="col-sm-12 text-right">
                    {{ $cc->links() }}
                </div>
            </div>
        </div>
      </div>

       @include('website.includes.conpany-detail') @include('website.includes.include_javascripts.architecture')
        
        @include('website.includes.get-quotation')
        @include('website.includes.google_recaptcha') 
        @endsection 
@push('script')

@include('website.includes.company-detail-and-get-quotation-script')
  
@endpush