<div class="widget-body form">
                            <!-- BEGIN FORM-->

                            <form class="cmxform form-horizontal" id="commentForm" method="get" action="#">
                                <div class="control-group ">
                                    <label for="cname" class="control-label">Name (required)</label>
                                    <div class="controls">
                                        <input class="span6 " id="cname" name="name" minlength="2" type="text" required />
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="cemail" class="control-label">E-Mail (required)</label>
                                    <div class="controls">
                                        <input class="span6 " id="cemail" type="email" name="email" required />
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="curl" class="control-label">URL (optional)</label>
                                    <div class="controls">
                                        <input class="span6 " id="curl" type="url" name="url" />
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="ccomment" class="control-label">Your Comment (required)</label>
                                    <div class="controls">
                                        <textarea class="span6 " id="ccomment" name="comment" required></textarea>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button class="btn btn-success" type="submit">Save</button>
                                    <button class="btn" type="button">Cancel</button>
                                </div>


                            </form>
                            <!-- END FORM-->
                        </div>