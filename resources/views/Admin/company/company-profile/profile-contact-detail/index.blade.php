@extends('Admin.admin-layout')

@push('css')
@endpush
@section('content')
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->   

   <!-- BEGIN EDITABLE TABLE widget-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE widget-->
         <div class="widget purple">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i> Editable Table</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
            </div>
            <div class="widget-body">
               <div>
                  <div class="clearfix">
                     <div class="btn-group">
                        <a href="{{ route('company-profile-contact.create') }}" id="editable-sample_new" class="btn green">
                        Add New <i class="icon-plus"></i>
                        </a>
                     </div>
               
                  </div>
                  <div class="space15"></div>
                  <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">
                     <div class="row-fluid">
                        <div class="span6">
                           <div id="editable-sample_length" class="dataTables_length">
                              <label>
                                 <select size="1" name="editable-sample_length" aria-controls="editable-sample" class="xsmall">
                                    <option value="5" selected="selected">5</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="-1">All</option>
                                 </select>
                                 records per page
                              </label>
                           </div>
                        </div>
                        <div class="span6">
                           <div class="dataTables_filter" id="editable-sample_filter"><label>Search: <input type="text" aria-controls="editable-sample" class="medium"></label></div>
                        </div>
                     </div>
                     <table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
                        <thead>
                           <tr role="row">
                              <th>#</th>
                              <th>Company Profile</th>
                              <th>Company Fullname</th>
                              <th>Company Creation date</th>
                              <th>Company Address</th>
                              <th>Company Owner Name</th>
                              <th>Company Mobile Number</th>
                              <th>Company Email</th>
                              <th>Edit</th>
                              <th>Delete</th>
                           </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                               @foreach($company_profile_contact as $index => $value)
                           <tr class="odd">
                              <td class="  sorting_1">{{ ++$index }}</td>
                              <td class=" ">{{ $value->company['c_name'] }}</td>
                              <td class=" ">{{ $value->c_fullname }}</td>
                              <td class="center ">{{ $value->c_created_at }}</td>
                              <td class="center ">{{ $value->c_address }}</td>
                              <td class="center ">{{ $value->c_owner_name }}</td>
                              <td class="center ">{{ $value->c_mobile_number }}</td>
                              <td class="center ">{{ $value->c_email }}</td>
                              
                              <td class=" "><a class="edit" href="{{ route('company-profile-contact.edit',$value->id) }}">Edit</a></td>
                              <td class=" "><a class="delete" href="{{ route('company-profile-contact.destroy',$value->id) }}">Delete</a></td>
                           </tr>
                           @endforeach
                           
                        </tbody>
                     </table>
                     <div class="row-fluid">
                        <div class="span6">
                           <div class="dataTables_info" id="editable-sample_info">Showing 1 to 5 of 12 entries</div>
                        </div>
                        <div class="span6">
                           <div class="dataTables_paginate paging_bootstrap pagination">
                              <ul>
                                 <li class="prev disabled"><a href="#">← Prev</a></li>
                                 <li class="active"><a href="#">1</a></li>
                                 <li><a href="#">2</a></li>
                                 <li><a href="#">3</a></li>
                                 <li class="next"><a href="#">Next → </a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- END EXAMPLE TABLE widget-->
      </div>
   </div>
   <!-- END EDITABLE TABLE widget-->
</div>
@endsection

@push('js')
@endpush