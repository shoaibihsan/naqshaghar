@extends('Admin.admin-layout')

@push('css')
@endpush
@section('content')

<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->
   <div class="row-fluid">
   
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN VALIDATION STATES-->
         <div class="widget red">
            <div class="widget-title">
               <h4><i class=" icon-key"></i> Landscape </h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
            </div>
            <div class="widget-body form">
               <!-- BEGIN FORM-->
                    @if(isset($arch) && !empty($arch))
                  <form  class="form-horizontal" method="post" action="{{ route('Landscape.update',$arch->id) }}">
                     @method('put')
               @else
               <form  class="form-horizontal" method="post" action="{{ route('Landscape.store') }}">
                  @endif
                  @csrf
               <div class="control-group success">
                     <label class="control-label" for="c_id">Company </label>
                     <div class="controls">
                        <select class="span6 form-control" name="company_id" id="company_id" required="">
                           <option value="">Please Select Company</option>
                           @foreach($company as $value)
                              <option value="{{ $value->id }}" @if(isset($arch) && isset($arch) && $value->id = $arch->company_id) selected="selected" @endif>{{ $value->c_name }}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="inputError">plot_location</label>
                     <div class="controls">
                         <select name="plot_location" class="form-control">
                                        <option value="">Please select Plot Location</option>
                                        <option value="Jinnah Colony, Faisalabad">Jinnah Colony, Faisalabad</option>
                                        <option value="Batala Colony, Faisalabad">Batala Colony, Faisalabad</option>
                                        <option value="DC Colony, Gujranwala">DC Colony, Gujranwala</option>
                                        <option value="Rahwali, Gujranwala">Rahwali, Gujranwala</option>
                                        <option value="Canal View, Gujranwala">Canal View, Gujranwala</option>
                                        <option value="City Housing Society, Gujranwala">City Housing Society, Gujranwala</option>
                                        <option value="Affandi colony, Islamabad">Affandi colony, Islamabad</option>
                                        <option value="Burma Town, Islamabad">Burma Town, Islamabad</option>
                                        <option value="Garden town, Islamabad">Garden town, Islamabad</option>
                                        <option value="Jagoit, Islamabad">Jagoit, Islamabad</option>
                                        <option value="Saddar, Islamabad">Saddar, Islamabad</option>
                                        <option value="Sector E-11, Islamabad">Sector E-11, Islamabad</option>
                                        <option value="Sector F-11, Islamabad">Sector F-11, Islamabad</option>
                                        <option value="Sector F-7, Islamabad">Sector F-7, Islamabad</option>
                                    </select>
                     
                  
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="inputError">covered_area</label>
                     <div class="controls">
                         <select name="covered_area" class="form-control">
                                            <option value="">Please select no of floor</option>
                                            <option value="1000">1000</option>

                                        </select>
                      
                       
                     </div>
                  </div>

                  <div class="control-group error">
                     <label class="control-label" for="inputError">plot_type</label>
                     <div class="controls">
                         <select name="plot_type" class="form-control">
                                            <option value="">Please select Type</option>
                                            <option value="Residential">Residential </option>
                                            <option value="Commercial">Commercial</option>

                                        </select>
                      
                       
                     </div>
                  </div>
                  
                   <div class="control-group error">
                     <label class="control-label" for="inputError">rate_sqft</label>
                     <div class="controls">
                          <select name="rate_sqft" class="form-control">
                                            <option value="">Please select Rate</option>
                                            <option value="5 Marla">5 Marla</option>
                                            <option value="10 Marla">10 Marla</option>
                                            <option value="1 kanal">1 kanal</option>
                                            <option value="2 kanal">2 kanal</option>

                                        </select>
                       
                        
                     </div>
                  </div>
                   <div class="control-group error">
                     <label class="control-label" for="inputError">total</label>
                     <div class="controls">
                          <select name="   total" class="form-control">
                                            <option value="">Please select Type</option>
                                            <option value="Residential">Residential </option>
                                            <option value="Commercial">Commercial</option>

                                        </select>
                        
                        
                     </div>
                  </div>
                   <div class="control-group error">
                     <label class="control-label" for="inputError">Professional Location</label>
                     <div class="controls">
                         <select name="professional_location" class="form-control">
                                            <option value="">Please select Professional location</option>
                                            <option value="Quaid-e-Azam Industrial Estate, Lahore">Quaid-e-Azam Industrial Estate, Lahore</option>
                                            <option value="Royal Orchard, Multan">Royal Orchard, Multan</option>
                                            <option value="Shah Rukn-e-Alam Colony, Multan">Shah Rukn-e-Alam Colony, Multan</option>
                                            <option value="Model Town, Multan">Model Town, Multan</option>
                                            <option value="Chowk Shaheedan, Multan">Chowk Shaheedan, Multan</option>
                                        </select>
                        
                     </div>
                  </div>
                
                   <div class="control-group error">
                     <label class="control-label" for="inputError">details</label>
                     <div class="controls">
                        <input type="text" class="span6" id="inputError" required="" name="details" @if(isset($arch->details) && !empty($arch->details)) value="{{ $arch->details }}" @endif>
                        
                     </div>
                  </div>
                     <div class="control-group error">
                     <label class="control-label" for="inputError">get_quotation</label>
                     <div class="controls">
                        <input type="text" class="span6" id="inputError" name="get_quotation" required="" @if(isset($arch->get_quotation) && !empty($arch->get_quotation)) value="{{ $arch->get_quotation }}" @endif>
                       
                     </div>
                  </div>
                               
                  <div class="form-actions">
                     <button type="submit" class="btn btn-success">Save</button>
                     <button type="button" class="btn">Cancel</button>
                  </div>
               </form>
               <!-- END FORM-->
            </div>
         </div>
         <!-- END VALIDATION STATES-->
      </div>
   </div>

</div>
@endsection

@push('js')
@endpush