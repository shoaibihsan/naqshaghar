
<style type="text/css">
   .p-9{
      padding: 9px;
   }
</style>
<!-- BEGIN SIDEBAR -->
<div class="sidebar-scroll">
   <div id="sidebar" class="nav-collapse collapse">
      <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
      <div class="navbar-inverse">
         <form class="navbar-search visible-phone">
            <input type="text" class="search-query" placeholder="Search" />
         </form>
      </div>
      <!-- END RESPONSIVE QUICK SEARCH FORM -->
      <!-- BEGIN SIDEBAR MENU -->
      <ul class="sidebar-menu">
         <li class="sub-menu @if(Request::is('Admin')) active @endif">
            <a class="p-9" href="{{url('Admin')}}">
            <i class="icon-dashboard"></i>
            <span>Dashboard</span>
            </a>
         </li>
         <li class="sub-menu @if(Request::is('Admin/Architecture') || Request::is('Admin/Architecture/*') || Request::is('Admin/construction') || Request::is('Admin/construction/*') || Request::is('Admin/Interior') || Request::is('Admin/Interior/*') || Request::is('Admin/TownPlaning') || Request::is('Admin/TownPlaning/*') || Request::is('Admin/Landscape') || Request::is('Admin/Landscape/*')) active open @endif">
            <a href="javascript:;" class="p-9">
            <i class="icon-book"></i>
            <span>Main Menu</span>
            <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li class="@if(Request::is('Admin/Architecture') || Request::is('Admin/Architecture/*')) active @endif">
                  <a class="p-9" href="{{url('Admin/Architecture')}}">
                     Architecture
                  </a>
               </li>
               <li class="@if(Request::is('Admin/construction') || Request::is('Admin/construction/*')) active @endif">
                  <a class="p-9" href="{{url('Admin/construction')}}">
                     Construction
                  </a>
               </li>
               <li class="@if(Request::is('Admin/Interior') || Request::is('Admin/Interior/*')) active @endif">
                  <a class="p-9" href="{{url('Admin/Interior')}}">
                     Interior
                  </a>
               </li>
               <li class="@if(Request::is('Admin/Landscape') || Request::is('Admin/Landscape/*')) active @endif">
                  <a class="p-9" href="{{url('Admin/Landscape')}}">
                     LandScape
                  </a>
               </li>
               <li class="@if(Request::is('Admin/TownPlaning') || Request::is('Admin/TownPlaning/*')) active @endif">
                  <a class="p-9" href="{{url('Admin/TownPlaning')}}">
                     Town Planiing
                  </a>
               </li>
             
            </ul>
         </li>

          <li class="sub-menu  @if(Request::is('admin/company') || Request::is('admin/company/*') || Request::is('admin/company-profile') || Request::is('admin/company-profile/*') || Request::is('admin/company-profile-contact') || Request::is('admin/company-profile-contact/*') || Request::is('admin/company-profile-gallery') || Request::is('admin/company-profile-gallery/*')) active open @endif">
            <a href="javascript:;" class="p-9">
            <i class="icon-book"></i>
            <span>Company</span>
            <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li  class="@if(Request::is('admin/company') || Request::is('admin/company/*')) active @endif">
                  <a class="p-9" href="{{ route('company.index') }}">
                     Compnay
                  </a>
               </li>
               <li class="@if(Request::is('admin/company-profile') || Request::is('admin/company-profile/*')) active @endif">
                  <a class="p-9" href="{{ route('company-profile.index') }}">
                     Company Profile
                  </a>
               </li>
               <li class="@if(Request::is('admin/company-profile-contact') || Request::is('admin/company-profile-contact/*')) active @endif">
                  <a class="p-9" href="{{ route('company-profile-contact.index') }}">
                     Compnay profile Contact
                  </a>
               </li>
               <li class="@if(Request::is('admin/company-profile-gallery') || Request::is('admin/company-profile-gallery/*')) active @endif">
                  <a class="p-9" href="{{ route('company-profile-gallery.index') }}">
                  Company Profile Gallery 
               </a>
            </li>
            </ul>
         </li>
      </ul>
      <!-- END SIDEBAR MENU -->
   </div>
</div>
<!-- END SIDEBAR -->
