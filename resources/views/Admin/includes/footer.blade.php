<div id="footer">
   2013 &copy; Metro Lab Dashboard.
</div>


   <script src="{{ asset('public/admin-assets')}}/js/jquery-1.8.3.min.js"></script>
   <script src="{{ asset('public/admin-assets')}}/js/jquery.nicescroll.js" type="text/javascript"></script>
   <script type="text/javascript" src="{{ asset('public/admin-assets')}}/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
   <script type="text/javascript" src="{{ asset('public/admin-assets')}}/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
   <script src="{{ asset('public/admin-assets')}}/assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
   <script src="{{ asset('public/admin-assets')}}/assets/bootstrap/js/bootstrap.min.js"></script>


   <script src="{{ asset('public/admin-assets')}}/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
   <script src="{{ asset('public/admin-assets')}}/js/jquery.sparkline.js" type="text/javascript"></script>
   <script src="{{ asset('public/admin-assets')}}/assets/chart-master/Chart.js"></script>
   <script src="{{ asset('public/admin-assets')}}/js/jquery.scrollTo.min.js"></script>


   <!--common script for all pages-->
   <script src="{{ asset('public/admin-assets')}}/js/common-scripts.js"></script>

   <!--script for this page only-->

   <script src="{{ asset('public/admin-assets')}}/js/easy-pie-chart.js"></script>
   <script src="{{ asset('public/admin-assets')}}/js/sparkline-chart.js"></script>
   <script src="{{ asset('public/admin-assets')}}/js/home-page-calender.js"></script>
   <script src="{{ asset('public/admin-assets')}}/js/home-chartjs.js"></script>

   <!-- END JAVASCRIPTS -->   
</body>

</html>