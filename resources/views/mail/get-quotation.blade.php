<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table { 
	width: 750px; 
	border-collapse: collapse; 
	margin:50px auto;
	}

/* Zebra striping */
tr:nth-of-type(odd) { 
	background: #eee; 
	}

th { 
	background: #3498db; 
	color: white; 
	font-weight: bold; 
	}

td, th { 
	padding: 10px; 
	border: 1px solid #ccc; 
	text-align: left; 
	font-size: 18px;
	}

/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

	table { 
	  	width: 100%; 
	}

	/* Force table to not be like tables anymore */
	table, thead, tbody, th, td, tr { 
		display: block; 
	}
	
	/* Hide table headers (but not display: none;, for accessibility) */
	thead tr { 
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
	
	tr { border: 1px solid #ccc; }
	
	td { 
		/* Behave  like a "row" */
		border: none;
		border-bottom: 1px solid #eee; 
		position: relative;
		padding-left: 50%; 
	}

	td:before { 
		/* Now like a table header */
		position: absolute;
		/* Top/left values mimic padding */
		top: 6px;
		left: 6px;
		width: 45%; 
		padding-right: 10px; 
		white-space: nowrap;
		/* Label the data */
		content: attr(data-column);

		color: #000;
		font-weight: bold;
	}

}
	</style>
</head>
<body><h5>This is New Quotation</h5>
	<table>
  <thead>
    <tr>
      <th>Name:</th>
      <th>Email</th>
      <th>Mobile Number</th>
      <th>Company Id</th>
      <th>Company Name</th>
      <th>Company Rate</th>
    </tr>
  </thead>
  <tbody>
      
    <tr>
      <td data-column="{{ $data['full_name'] }}">{{ $data['full_name'] }}</td>
      <td data-column="{{ $data['email'] }}">{{ $data['email'] }}</td>
      <td data-column="{{ $data['mobile'] }}">{{ $data['mobile'] }}</td>
      <td data-column="{{ $data['company_id'] }}">{{ $data['company_id'] }}</td>
      <td data-column="{{ $data['company_name'] }}">{{ $data['company_name'] }}</td>
      <td data-column="{{ $data['company_rate'] }}">{{ $data['company_rate'] }}</td>
    </tr>
    
  </tbody>
</table>
</body>
</html>